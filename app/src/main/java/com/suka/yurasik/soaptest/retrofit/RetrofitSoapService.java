package com.suka.yurasik.soaptest.retrofit;

import android.support.annotation.NonNull;

import com.suka.yurasik.soaptest.model.response.ResponseEnvelope;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface RetrofitSoapService {
    @Headers({
            "Content-Type: application/soap+xml",
            "Accept-Charset: utf-8",
            "SOAPAction: \"\""
    })
    @POST("VeroMobileWebService/services/VeroMobileWebService")
    Call<ResponseEnvelope> postQuery(@NonNull @Body RequestBody body);
}
