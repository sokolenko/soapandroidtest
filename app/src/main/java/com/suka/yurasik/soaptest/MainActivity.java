package com.suka.yurasik.soaptest;

import android.graphics.Point;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.suka.yurasik.soaptest.model.MobEntry;
import com.suka.yurasik.soaptest.model.request.MobRequestBody;
import com.suka.yurasik.soaptest.model.request.MobRequestData;
import com.suka.yurasik.soaptest.model.request.RequestEnvelope;
import com.suka.yurasik.soaptest.model.response.ResponseEnvelope;
import com.suka.yurasik.soaptest.retrofit.RetrofitSoapService;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.convert.AnnotationStrategy;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.strategy.Strategy;

import java.io.StringWriter;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class MainActivity extends AppCompatActivity {
    private final static String TAG = "Activity";
    protected RetrofitSoapService service;
    protected Serializer serializer;

    // Elements of a SOAP envelop body.
    private String NAMESPACE = "http://mobile.webservices.vas.vero.com";
    private String METHOD_NAME = "uploadImageToVehicle";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        Strategy strategy = new AnnotationStrategy();
        serializer = new Persister(strategy);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://94.130.193.252:5800/")
                .client(client)
                .addConverterFactory(SimpleXmlConverterFactory.create(serializer))
                .build();
        service = retrofit.create(RetrofitSoapService.class);

       /* SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER12);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        String action = NAMESPACE + "/" + METHOD_NAME;
        d("*************************************************");
        d("1: " + request);
        d("2: " + envelope.env);
        d("3: " + envelope.env);
        d("4: " + envelope.env);
        d("5: " + envelope.env);*/



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                MobEntry vsn = new MobEntry();
                vsn.setText("???");
                MobEntry image = new MobEntry();
                image.setText("?");
                MobEntry fileName = new MobEntry();
                fileName.setText("?");
                MobRequestData mobRequestData = new MobRequestData(vsn, image,fileName);
                MobRequestBody mobRequestBody = new MobRequestBody();
                mobRequestBody.setData(mobRequestData);
                RequestEnvelope requestEnvelope = new RequestEnvelope();
                requestEnvelope.setBody(mobRequestBody);

                StringWriter result=new StringWriter();
                try {
                    serializer.write(requestEnvelope,result);
                }
                catch (  Exception e) {
                    e.printStackTrace();
                }

                RequestBody requestBody = RequestBody.create(MediaType.parse("text/xml;charset=utf-8"), result.toString());

                Call<ResponseEnvelope> call = service.postQuery(requestBody);
                call.enqueue(new Callback<ResponseEnvelope>() {
                    @Override
                    public void onResponse(Call<ResponseEnvelope> call, Response<ResponseEnvelope> response) {
                        if (response.body() != null) {
                            d("Response: " + response.body().getBody().getResponseData().getResult());
                        } else {
                            d("Response: " + response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseEnvelope> call, Throwable t) {
                        d("Error!");
                        t.printStackTrace();
                    }
                });

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void d(String message) {
        Log.d(TAG, message);
    }
}
