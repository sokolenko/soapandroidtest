package com.suka.yurasik.soaptest.model;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

@Root(name = "mob", strict = false)
@Namespace(reference = "http://mobile.webservices.vas.vero.com", prefix = "mob")
public class MobEntry {

    @Text
    protected String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
