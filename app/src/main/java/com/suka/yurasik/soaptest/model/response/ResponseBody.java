package com.suka.yurasik.soaptest.model.response;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

/*
<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <soapenv:Body>
        <uploadImageToVehicleResponse xmlns="http://mobile.webservices.vas.vero.com">
          <uploadImageToVehicle>Success</uploadImageToVehicle>
        </uploadImageToVehicleResponse>
   </soapenv:Body>
</soapenv:Envelope>
 */

@Root(name = "Body", strict = false)
@Namespace(reference = "http://schemas.xmlsoap.org/soap/envelope/")
public class ResponseBody {

    @Element(name = "uploadImageToVehicleResponse", required = false)
    @Namespace(reference = "http://mobile.webservices.vas.vero.com")
    ResponseData responseData;

    private ResponseBody(@Element(name = "uploadImageToVehicleResponse") ResponseData responseData) {
        this.responseData = responseData;
    }

    public ResponseData getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseData responseData) {
        this.responseData = responseData;
    }

    @Override
    public String toString() {
        return "ResponseBody{" +
                "responseData=" + responseData +
                '}';
    }
}