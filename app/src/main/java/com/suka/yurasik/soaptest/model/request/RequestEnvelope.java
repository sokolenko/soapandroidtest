package com.suka.yurasik.soaptest.model.request;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.NamespaceList;
import org.simpleframework.xml.Root;
// xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:mob="http://mobile.webservices.vas.vero.com">

@Root(name = "soapenv:Envelope")
@NamespaceList({
        @Namespace(reference = "http://schemas.xmlsoap.org/soap/envelope/", prefix = "soapenv"),
        @Namespace(reference = "http://mobile.webservices.vas.vero.com", prefix = "mob")
})
public class RequestEnvelope {
    @Element(name = "soapenv:Body",  required = false)
    protected MobRequestBody body;
    public MobRequestBody getBody() {
        return body;
    }
    public void setBody(MobRequestBody body) {
        this.body = body;
    }
}
