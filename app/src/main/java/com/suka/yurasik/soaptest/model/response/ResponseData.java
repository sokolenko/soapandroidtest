package com.suka.yurasik.soaptest.model.response;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

@Root(name = "uploadImageToVehicleResponse", strict = false)
public class ResponseData {

    @Element(name = "uploadImageToVehicle")
    String result;

    public ResponseData(@Element(name = "uploadImageToVehicle") String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "ResponseData{" +
                "result='" + result + '\'' +
                '}';
    }
}