package com.suka.yurasik.soaptest.model.response;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

/*
<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <soapenv:Body>
        <uploadImageToVehicleResponse xmlns="http://mobile.webservices.vas.vero.com">
          <uploadImageToVehicle>Success</uploadImageToVehicle>
        </uploadImageToVehicleResponse>
   </soapenv:Body>
</soapenv:Envelope>
 */

@Root(name = "Envelope")
@Namespace(prefix = "soapenv", reference = "http://schemas.xmlsoap.org/soap/envelope/")
public class ResponseEnvelope {

    @Element(name = "Body", required = false)
    @Namespace(prefix = "soapenv", reference = "http://schemas.xmlsoap.org/soap/envelope/")
    ResponseBody body;

    public ResponseEnvelope(@Element(name = "Body") ResponseBody body) {
        this.body = body;
    }

    public ResponseBody getBody() {
        return body;
    }

    public void setBody(ResponseBody body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "ResponseEnvelope{" +
                "body=" + body +
                '}';
    }
}
