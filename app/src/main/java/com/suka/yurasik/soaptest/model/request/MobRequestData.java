package com.suka.yurasik.soaptest.model.request;

import com.suka.yurasik.soaptest.model.MobEntry;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;
@Root(name = "mob", strict = false)
@Namespace(reference = "http://mobile.webservices.vas.vero.com", prefix = "mob")
public class MobRequestData {
     /*<mob:vsn>fdsdsf</mob:vsn>
         <mob:imageString>?</mob:imageString>
         <mob:fileName>?</mob:fileName>*/
    @Element(name = "vsn")
    protected MobEntry user;

    @Element(name = "imageString")
    protected MobEntry image;

    @Element(name = "fileName")
    protected MobEntry fileName;

    public MobRequestData() {
    }

    public MobRequestData(MobEntry user, MobEntry image, MobEntry fileName) {
        this.user = user;
        this.image = image;
        this.fileName = fileName;
    }

    public MobEntry getUser() {
        return user;
    }

    public void setUser(MobEntry user) {
        this.user = user;
    }

    public MobEntry getImage() {
        return image;
    }

    public void setImage(MobEntry image) {
        this.image = image;
    }

    public MobEntry getFileName() {
        return fileName;
    }

    public void setFileName(MobEntry fileName) {
        this.fileName = fileName;
    }
}
