package com.suka.yurasik.soaptest.model.request;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "soapenv:Body", strict = false)
public class MobRequestBody {

    @Element(name = "uploadImageToVehicle", required = false)
    protected MobRequestData data;

    public MobRequestData getData() {
        return data;
    }

    public void setData(MobRequestData data) {
        this.data = data;
    }
}
